var NaturalLanguageUnderstandingV1 = require('watson-developer-cloud/natural-language-understanding/v1.js');

/*
credentials = {
  'username':'<your-username>',
  'password':'<your-password>',
  'versionDate':'<version-date>'
}
*/

module.exports = function(credentials){

	var username = credentials.username;
	var password = credentials.password;
	var versionDate = credentials.versionDate;

	var natural_language_understanding = new NaturalLanguageUnderstandingV1({
	  'username': username,
	  'password': password,
	  'version_date': versionDate
	});


	/*
	* Analyze
	*
	* @param {String} text
	* @param {Object} features
	*
	*/
	function analyze(text){

		return new Promise((resolve, reject) => {

		    var data = {
		    	'text': text,
				'features': {
					'entities': {},
					'keywords': {},
					'categories':{},
					'concepts':{},
					'sentiment':{},
					'emotion':{},
					'semantic_roles':{}
				}
		    }

		    natural_language_understanding.analyze(data, function(err, response) {

				if (err){
				  reject(JSON.stringify(err));
				}
				
				resolve(JSON.stringify(response, null, 2));
			});
			
		});
	}


	function listModels(){
		return new Promise((resolve, reject) => {

			
			natural_language_understanding.listModels({}, function(err, response) {
			    if (err){
				  	reject(JSON.stringify(err));
				}
				
				resolve(JSON.stringify(response, null, 2));
			});
		});
	}


	/*
	* Delete a model
	*
	* @param {String} model
	*
	*/


	function deleteModel(id){

		return new Promise((resolve, reject) => {

			var requireData = [
		      	id
		      ];
	      
		    requireData.forEach(function(data){
		      	if(!data){
		        	reject({'require':'Id is require.'});
		      	}
		    });

			var data = {
			  	'model_id': id
			};

			
			natural_language_understanding.deleteModel(data, function(err, response) {
			    if (err){
				  	reject(JSON.stringify(err));
				}
				
				resolve(JSON.stringify(response, null, 2));
			});
		});
	}

	return {
		analyze: analyze,
		listModels: listModels,
		deleteModel: deleteModel
	}

}
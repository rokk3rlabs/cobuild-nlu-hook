# Cobuild NLU Hook  #

## Usage ##

### Analyze ###
```
#!javascript
	var username = credentials.username;
	var password = credentials.password;
	var versionDate = credentials.versionDate;

	var natural_language_understanding = new NaturalLanguageUnderstandingV1({
	  'username': username,
	  'password': password,
	  'version_date': versionDate
	});
	
	var data = {
		'text': text,
		'features': {
		'entities': {
			'emotion': features.entities.emotion,
			'sentiment': features.entities.sentiment,
			'limit': features.entities.limit
		},
		'keywords': {
			'emotion': features.keywords.emotion,
			'sentiment': features.keywords.sentiment,
			'limit': features.keywords.limit
		}
	}
	
	natural_language_understanding.analyze(data, function(err, response) {

		if (err){
			reject(err);
		}

		resolve(JSON.stringify(response, null, 2));
	});

```
## Test ##

`npm test`
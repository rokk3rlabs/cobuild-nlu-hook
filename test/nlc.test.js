var should = require('should');

describe('NLC Hook', function(){

	var fixtures = {};

	before(function (done) {
    	loadFixtures();
    	done();
  	});

  	it('Should analyze ', function (done) {
  		
	    var nlcHook = require(process.cwd()+'/index.js')(fixtures.credentials);
	    nlcHook.analyze(fixtures.parameters.text)
	    .then(function(response){
	      	done()
	    })
	    .catch(function(err){
	      	done()
	    })
	});


	it('Should list models', function(done){
		var nlcHook = require(process.cwd()+'/index.js')(fixtures.credentials);
	    nlcHook.listModels()
	    .then(function(response){
	    	done()
	    }).catch(function(err){
	    	console.log(err)
	    })

	})



  	function loadFixtures(argument) {
	    var Cobuild = require('cobuild2');
	    var path = require('path');

	    Cobuild.Utils.Files.listFilesInFolder(__dirname + '/fixtures')
	    .forEach(function (file) {
	    
	        var completePath =  path.join(__dirname , 'fixtures',  file)
	       
	        fixtures[file.slice(0, -3)] = require(completePath);
	        
	    });
	}

})